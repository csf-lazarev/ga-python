import datetime
import matplotlib.pyplot as plt
import numpy as np

from ga.ga_solver import *
from tps.solver import TPSCrossover, TPSMutation, tps_fitness
from matrices import *


def get_random_input(n, start_from=0):
    out = random.sample(range(n), n)
    out.remove(start_from)
    out.insert(0, start_from)
    return out


def get_genetic_result(matrix, max_entities, mutation_chance, epohs = 40000):
    N = len(matrix)
    start = [GAVector(get_random_input(N, 0)) for _ in range(N * 2)]
    solver = GASolver(
        TPSCrossover(N // 3),
        TPSMutation(),
        GASolver.RandomSelector(),
        fitness=lambda v: tps_fitness(matrix, v),
        max_entities=max_entities,
        mutation_chance=mutation_chance
    )
    ct = datetime.datetime.now()
    stat = solver.fit(start, max_epoch=epohs)
    time = datetime.datetime.now() - ct
    raw = np.array(solver.stat.best_entities[-1].x)
    return np.concatenate([raw, [raw[0]]]), time, stat
