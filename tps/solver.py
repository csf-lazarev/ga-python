import random

from ga.ga_solver import GASolver
from ga.ga_vector import GAVector


class TPSCrossover(GASolver.Crossover):
    def __init__(self, split):
        self.split = split

    def cross(self, a: GAVector, b: GAVector):
        def _do_cross(c, d):
            out = list()
            out += c[0:self.split]
            for e in d[self.split:]:
                if e not in out:
                    out.append(e)
            for e in c[self.split:]:
                if len(out) == len(c):
                    return GAVector(out)
                if e not in out:
                    out.append(e)
            return GAVector(out)

        return [_do_cross(a.x, b.x), _do_cross(b.x, a.x)]


class TPSMutation(GASolver.Mutation):
    def mutate(self, a: GAVector) -> GAVector:
        i, j = random.sample(range(1, len(a.x)), 2)
        out = a.x.copy()
        out[i], out[j] = out[j], out[i]
        return GAVector(out)


def tps_fitness(matrix, a: GAVector):
    sum = 0

    for i in range(len(a.x) - 1):
        sum += matrix[a.x[i]][a.x[i + 1]]
    sum += matrix[a.x[-1]][a.x[0]]

    return sum
