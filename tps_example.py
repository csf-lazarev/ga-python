from ga.ga_solver import *
from tps.solver import TPSCrossover, TPSMutation, tps_fitness

matrix = [
    [0, 25, 40, 31, 27],
    [5, 0, 17, 30, 25],
    [19, 15, 0, 6, 1],
    [9, 5, 24, 0, 6],
    [22, 8, 7, 10, 0]
]

# matrix = [
#     [0, 1, 40, 31, 27],
#     [5, 0, 1, 30, 25],
#     [19, 15, 0, 1, 1],
#     [9, 5, 24, 0, 1],
#     [1, 8, 7, 10, 0]
# ]

# for i in range(len(matrix)):
#     for j in range(i):
#         matrix[i][j] = matrix[j][i]

start = [
    GAVector([0, 3, 4, 1, 2]),
    GAVector([0, 1, 3, 4, 2]),
    GAVector([0, 2, 4, 1, 3]),
    GAVector([0, 3, 2, 4, 1]),
]

if __name__ == '__main__':
    solver = GASolver(
        TPSCrossover(2),
        TPSMutation(),
        GASolver.RandomSelector(),
        fitness=lambda v: tps_fitness(matrix, v),
        mutation_chance=60
    )

    answer = 50

    stat = solver.fit(start, max_epoch=30000, preferred_fitness=answer)
    stat1 = stat
    print(stat.best_entities[-1])
    print(stat.total_mutations)
    print(stat.epochs)

    start = stat.best_entities[-5:].copy()
    start += stat1.worst_entities[0:10].copy()

    print("=================")

    stat = solver.fit(start, max_epoch=20000, preferred_fitness=answer)
    print(stat.best_entities[-1])
    print(stat.total_mutations)
    print(stat.epochs)

    start = stat.best_entities[-5:].copy()
    start += stat1.worst_entities[0:10].copy()

    print("=================")

    stat = solver.fit(start, max_epoch=10000, preferred_fitness=answer)
    print(stat.best_entities[-1])
    print(stat.total_mutations)
    print(stat.epochs)
