import random
from random import sample

from ga.ga_solver import GASolver
from ga.ga_vector import GAVector


class SimpleCross(GASolver.Crossover):

    def __init__(self, split):
        self.split = split

    def cross(self, a: GAVector, b: GAVector):
        out1 = list()
        out1 += (a.x[0:self.split])
        out1 += (b.x[self.split:])

        out2 = list()
        out2 += (b.x[0:self.split])
        out2 += (a.x[self.split:])

        return [GAVector(out1), GAVector(out2)]


class SwapMutation(GASolver.Mutation):
    def mutate(self, a: GAVector) -> GAVector:
        ind = random.randint(0, len(a.x) - 1)
        val = random.randint(-10, 10)
        b = a.x.copy()
        b[ind] = val
        return GAVector(b)


