import random
import logging as log
from abc import abstractmethod, ABC

from ga.ga_vector import GAVector


class GASolver:
    class FitnessStatistic:
        def __init__(self):
            self.best_entities = list()
            self.worst_entities = list()
            self.total_mutations = 0
            self.best_fitness = None
            self.epochs = 0

    def __init__(self,
                 cross: 'Crossover',
                 mutation: 'Mutation',
                 selector: 'Selector',
                 fitness,
                 max_entities=10,
                 mutation_chance=5):
        self.cross = cross
        self.mutation = mutation
        self.selector = selector
        self.population = []
        self.fitness = fitness
        self.mutation_chance = mutation_chance
        self.epoch = 0
        self.max_entities = max_entities
        self.stat = GASolver.FitnessStatistic()
        log.debug("Calculate initial fitness")
        for gav in self.population:
            gav.y = self.fitness(gav)

    class Crossover(ABC):
        @abstractmethod
        def cross(self, a: GAVector, b: GAVector):
            pass

    class Mutation(ABC):
        @abstractmethod
        def mutate(self, a: GAVector) -> GAVector:
            pass

    class Selector(ABC):
        @abstractmethod
        def select(self, population: list[GAVector]):
            pass

    class RandomSelector(Selector):
        def select(self, population: list[GAVector]):
            lst = random.sample(population, (len(population) // 2) * 2)
            return zip(lst[::2], lst[1::2])

    def solve_epoch(self):
        log.info(f"Start epoch {self.epoch}...")
        # select parents from population
        parent_tuples = self.selector.select(self.population)

        # get children by cross implementation
        for pair in parent_tuples:
            children = self.cross.cross(pair[0], pair[1])
            for ch in children:
                child = ch
                # mutate if a chance hits
                if self._mutate():
                    child = self.mutation.mutate(child)
                    self.stat.total_mutations += 1
                child.y = self.fitness(child)
                self.population.append(child)

        # sort population by fintess: from least to higest
        self.population.sort(key=lambda gav: gav.y)

        new_population = []

        # kill the worst creatures
        for ch in self.population:
            if ch not in new_population:
                new_population.append(ch)
                if len(new_population) == self.max_entities:
                    break
        self.population = new_population
        log.info(f"End of epoch {self.epoch}")
        self.epoch += 1

    def _prepare_for_fit(self, start_population):
        self.stat = GASolver.FitnessStatistic()
        self.population = start_population
        for c in self.population:
            c.y = self.fitness(c)

        assert len(self.population) > 1
        self.population.sort(key=lambda gav: gav.y)
        self.stat.best_entities.append(self.population[0])
        self.stat.worst_entities.append(self.population[-1])
        self.stat.best_fitness = self.population[0].y

    def fit(self, start_population: list[GAVector], max_epoch=30, preferred_fitness=0):
        self.epoch = 0
        self._prepare_for_fit(start_population)
        while self.epoch < max_epoch and abs(self.stat.best_fitness) > preferred_fitness:
            self.solve_epoch()
            best = self.population[0]
            worst = self.population[-1]
            if self.stat.best_fitness > best.y:
                self.stat.best_fitness = best.y
            self.stat.best_entities.append(best)
            self.stat.worst_entities.append(worst)
        self.stat.epochs = self.epoch
        return self.stat

    def _mutate(self):
        return random.randrange(1, 100) <= self.mutation_chance
