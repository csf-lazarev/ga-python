class GAVector:
    def __init__(self, x):
        self.x = x
        self.y = None

    def __str__(self):
        return f"[{self.x} -> {self.y}]"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.x == other.x
