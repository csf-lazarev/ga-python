import numpy as np
from matplotlib import pyplot as plt
from scipy import spatial

from ant.ant import get_ant_result
from tps.tps import get_genetic_result


def cal_total_distance(routine):
    num_points, = routine.shape
    return sum([distance_matrix[routine[i % num_points], routine[(i + 1) % num_points]] for i in range(num_points)])


N = 15  # количество вершин
points_coordinate = np.random.rand(N, 2)  # генерация рандомных вершин

print("Координаты вершин:\n", points_coordinate[:N], "\n")

# вычисление матрицы расстояний между вершин
distance_matrix = spatial.distance.cdist(points_coordinate, points_coordinate, metric='euclidean')

ant_res = get_ant_result(cal_total_distance, distance_matrix, size_pop=20, max_iter=10)
ant_vector = ant_res[0]
ant_path = points_coordinate[ant_vector, :]
ant_time = ant_res[2].total_seconds()

# Easy way get more accuracy - increase epochs
gen_res = get_genetic_result(distance_matrix, max_entities=20, mutation_chance=20, epohs=40000)
gen_vector = gen_res[0]
gen_path = points_coordinate[gen_vector, :]
gen_y = gen_res[2].best_entities[-1].y
gen_time = gen_res[1].total_seconds()

fig, ax = plt.subplots(1, 2)
for index in range(0, len(ant_vector)):
    ax[0].annotate(ant_vector[index], (ant_path[index, 0], ant_path[index, 1]))
ax[0].plot(ant_path[:, 0],
           ant_path[:, 1], 'o-r')
ax[0].set_title("Ant: {:.4f} ({:}s)".format(ant_res[1], ant_time))

for index in range(0, len(gen_vector)):
    ax[1].annotate(gen_vector[index], (gen_path[index, 0], gen_path[index, 1]))
ax[1].plot(gen_path[:, 0],
           gen_path[:, 1], 'o-r')
ax[1].set_title("Gen: {:.4f} ({:}s)".format(gen_y, gen_time))

# изменение размера графиков
# plt.rcParams['figure.figsize'] = [20, 10]
plt.show()
