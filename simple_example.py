from ga.ga_solver import *
from simple.solver import SimpleCross, SwapMutation

if __name__ == '__main__':
    solver = GASolver(
        SimpleCross(2),
        SwapMutation(),
        GASolver.RandomSelector(),
        fitness=lambda v: (v.x[0] + 1) ** 2 + (v.x[1] - 9) ** 2 + (v.x[2] - 4) ** 2 + (v.x[3] - 1) ** 2,
        mutation_chance=70
    )

    start = [GAVector([0, -5, 1, 6]), GAVector([1, 3, 0, 2]), GAVector([10, 4, 30, -2]), GAVector([4, 3, -2, 1])]

    stat = solver.fit(start, max_epoch=2000)
    print(f"Solution: {stat.best_entities[-1]}")
    print(f"Total mutations: {stat.total_mutations}")
    print(f"Total epochs: {stat.epochs}")
